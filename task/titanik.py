import pandas as pd
import statistics

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    missing_mr = 0
    missing_miss = 0
    missing_mrs = 0
    mr_ages = []
    miss_ages = []
    mrs_ages = []
    for name,age in zip( df["Name"], df["Age"] ):
        if "Mr." in name:
            if pd.isnull( age ):
                missing_mr += 1
            else:
                mr_ages.append( age )
        elif "Miss" in name:
            if pd.isnull( age ):
                missing_miss += 1
            else:
                miss_ages.append( age )
        elif "Mrs." in name:
            if pd.isnull( age ):
                missing_mrs += 1
            else:
                mrs_ages.append( age )

    mr_median = statistics.median( mr_ages )
    miss_median = statistics.median( miss_ages )
    mrs_median = statistics.median( mrs_ages )
    return [("Mr.", missing_mr, mr_median), ("Mrs.", missing_mrs, mrs_median), ("Miss.", missing_miss, miss_median)]
